//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package nesting.simulations.examples;

import inet.linklayer.vlan.VlanTunnel;
import ned.DatarateChannel;
import nesting.node.ethernet.VlanEtherHostQ;
import nesting.node.sdn.NestingSDNController;
import nesting.node.ethernet.VlanEtherHostSched;
import nesting.node.ethernet.VlanEtherSwitchPreemptable;
import nesting.node.nesting.NestingStandardHost;


network SDN_TSN_Scenario
{
    types:
        channel C extends DatarateChannel
        {
            delay = 0.1us;
            datarate = 1Gbps;
        }
    submodules:
        switchA: VlanEtherSwitchPreemptable {
            parameters:
                @display("p=188,127");
            gates:
                ethg[9];
        }
        switchB: VlanEtherSwitchPreemptable {
            parameters:
                @display("p=515,127");
            gates:
                ethg[8];
        }
        switchC: VlanEtherSwitchPreemptable {
            parameters:
                @display("p=285,83");
            gates:
                ethg[5];
        }
        switchD: VlanEtherSwitchPreemptable {
            parameters:
                @display("p=429,76");
            gates:
                ethg[4];
        }
        switchE: VlanEtherSwitchPreemptable {
            parameters:
                @display("p=246,207");
            gates:
                ethg[4];
        }
        switchF: VlanEtherSwitchPreemptable {
            parameters:
                @display("p=468,207");
            gates:
                ethg[5];
        }
        workstation1: VlanEtherHostSched {
            @display("p=46,77");
        }
        workstation2: VlanEtherHostSched {
            @display("p=118,23");
        }
        backupServer: VlanEtherHostQ {
            @display("p=622,23");
        }
        robotController: VlanEtherHostSched {
            @display("p=54,150");
        }
        roboticArm: VlanEtherHostQ {
            @display("p=640,274");
        }
        sdnController: NestingSDNController {
            parameters:
                @display("p=329,166");
            gates:
                ethg[6];
        }
        //deneme: VlanEtherHostQ_deneme {
        //    parameters:
        //    	@display("p=68,166");
        //    gates:
        //        ethg[2];	
        //}
        robotController2: VlanEtherHostSched {
            @display("p=77,214");
        }
        robotController3: VlanEtherHostSched {
            @display("p=149,274");
        }
        roboticArm2: VlanEtherHostQ {
            @display("p=640,189");
        }
        roboticArm3: VlanEtherHostQ {
            @display("p=640,116");
        }
    connections:
        robotController.ethg <--> C <--> switchA.ethg[0];
        robotController2.ethg <--> C <--> switchA.ethg[7];
        robotController3.ethg <--> C <--> switchA.ethg[8];
        workstation1.ethg <--> C <--> switchA.ethg[1];
        workstation2.ethg <--> C <--> switchA.ethg[2];
        //deneme.ethg[0] <--> C <--> switchA.ethg[6];
        //deneme.ethg[1] <--> C <--> switchE.ethg[3];

        roboticArm.ethg <--> C <--> switchB.ethg[0];
        roboticArm2.ethg <--> C <--> switchB.ethg[6];
        roboticArm3.ethg <--> C <--> switchB.ethg[7];
        backupServer.ethg <--> C <--> switchB.ethg[1];

        switchA.ethg[3] <--> C <--> switchB.ethg[2];
        switchA.ethg[4] <--> C <--> switchC.ethg[0];
        switchC.ethg[1] <--> C <--> switchD.ethg[0];
        switchC.ethg[2] <--> C <--> switchE.ethg[0];
        switchE.ethg[1] <--> C <--> switchF.ethg[0];
        switchF.ethg[1] <--> C <--> switchB.ethg[3];
        switchD.ethg[1] <--> C <--> switchF.ethg[2];
        switchC.ethg[3] <--> C <--> switchF.ethg[3];
        switchA.ethg[6] <--> C <--> switchE.ethg[3];
        switchB.ethg[5] <--> C <--> switchD.ethg[3];

        sdnController.ethg[0] <--> C <--> switchA.ethg[5];
        sdnController.ethg[1] <--> C <--> switchB.ethg[4];
        sdnController.ethg[2] <--> C <--> switchC.ethg[4];
        sdnController.ethg[3] <--> C <--> switchD.ethg[2];
        sdnController.ethg[4] <--> C <--> switchE.ethg[2];
        sdnController.ethg[5] <--> C <--> switchF.ethg[4];

}
