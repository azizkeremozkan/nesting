//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EthernetLayerApp.h"

namespace nesting {

Define_Module(EthernetLayerApp);

void EthernetLayerApp::initialize()
{
    // TODO - Generated method body
    fdb = getModuleFromPar<FilteringDatabase>(par("filteringDatabaseModule"), this);
    packetLength = &par("packetLength");
    vlanTagEnabled = &par("vlanTagEnabled");
    pcp = &par("pcp");
    dei = &par("dei");
    vid = &par("vid");
    useSNAP = par("useSNAP");
    fcsMode = parseFcsMode(par("fcsMode"));

    simtime_t delay;

    // komşuluk bilgisi için bunu gönderelim, tip 1
    scheduleAt(simTime(), new cMessage("selfMessage-1", 1));

    // arıza bildirmek için de bunu gönderelim, tip 2
    // switchD göndermiş olsun
    std::string switchName = this->getModuleByPath(par("switchModule"))->getFullName();
    if(switchName == "switchD"){
        delay = SimTime(40, SIMTIME_MS);
        delay += SimTime(300, SIMTIME_US);
        scheduleAt(simTime() + delay, new cMessage("selfMessage-2", 2));
    }

    // istatistik bildirmek için de bu mesajı periyodik olarak gönderelim, tip 3
    delay = SimTime(1, SIMTIME_MS);
    scheduleAt(simTime() + delay, new cMessage("selfMessage-3", 3));
}

void EthernetLayerApp::handleMessage(cMessage *msg)
{
    // TODO - Generated method body
    EV_INFO << "EthernetLayerApp handleMessage icinde" <<  std::endl;
    if (msg->isSelfMessage())
    {
        EV_INFO << "EthernetLayerApp self message icinde" <<  std::endl;
        if(msg->getKind() == 1){
            sendMessageToSdnController(1001);
        } else if(msg->getKind() == 2){
            sendMessageToSdnController(1002);
        } else if(msg->getKind() == 3){
            sendMessageToSdnController(1003);
            simtime_t delay = SimTime(1, SIMTIME_MS);
            scheduleAt(simTime() + delay, new cMessage("selfMessage-3", 3));
        }
    }
    else
    {
        EV_INFO << "EthernetLayerApp self message icinde degil" <<  std::endl;
        handleSdnRequest(msg);
    }
}

void EthernetLayerApp::sendMessageToSdnController(short iletiTipi)
{
    const char *destAddressVar = par("destAddress");
    if(strlen(destAddressVar) == 0)
    {
        EV_INFO << "Destination address yok" << std::endl;
    }
    else
    {
        char msgname[40] = "Message to SDN Controller";
        Packet *datapacket = new Packet(msgname, IEEE802CTRL_DATA);
        datapacket->setKind(iletiTipi); // komşuluk bilgisi için 1001 numaralı tip, arıza için 1002, istatistik için 1003
        long len = packetLength->intValue();
        const auto& payload = makeShared<ByteCountChunk>(B(len));
        // set creation time
        auto timeTag = payload->addTag<CreationTimeTag>();
        timeTag->setCreationTime(simTime());

        datapacket->insertAtBack(payload);
        datapacket->removeTagIfPresent<PacketProtocolTag>();
        datapacket->addTagIfAbsent<PacketProtocolTag>()->setProtocol(VlanEtherTrafGenSched::L2_PROTOCOL);
        // TODO check which protocol to insert
        auto sapTag = datapacket->addTagIfAbsent<Ieee802SapReq>();
        sapTag->setSsap(ssap);
        sapTag->setDsap(dsap);

        // create control info for encap modules
        auto macTag = datapacket->addTag<MacAddressReq>();
        destMacAddress = resolveDestMacAddress();
        macTag->setDestAddress(destMacAddress);

        // create VLAN control info
        if (vlanTagEnabled->boolValue()) {
            EnhancedVlanReq* vlanReq = datapacket->addTag<EnhancedVlanReq>();
            vlanReq->setPcp(pcp->intValue());
            vlanReq->setDe(dei->boolValue());
            vlanReq->setVlanId(vid->intValue());
        }

        EV_TRACE << getFullPath() << ": Send packet `" << datapacket->getName()
                        << "' dest=" << macTag->getDestAddress() << " length="
                        << datapacket->getBitLength() << "B type="
                        << IEEE802CTRL_DATA << " vlan-tagged="
                        << vlanTagEnabled->boolValue();
        if (vlanTagEnabled->boolValue()) {
            EV_TRACE << " pcp=" << pcp->intValue() << " dei=" << dei->boolValue() << " vid=" << vid->intValue();
        }
        EV_TRACE << endl;

        //emit(packetSentSignal, datapacket);
        processPacketFromHigherLayer(datapacket);
        send(datapacket, "out");
    }
}

void EthernetLayerApp::handleSdnRequest(cMessage *msg){
    Packet* packet = check_and_cast<Packet*>(msg);
    if(packet->getKind() == 1002){
        EV_INFO << "Sdn denetleyiciden kural değişikliği talebi aldı, tip 1002" << std::endl;
        // burada akış kuralları güncellensin
        // bu anahtarın filteringDatabaseModule nesnesi içinde güncelleme fonksiyonu çalıştır
        fdb->updateFlowRule();
    }
}

void EthernetLayerApp::processPacketFromHigherLayer(Packet *packet)
{
    delete packet->removeTagIfPresent<DispatchProtocolReq>();
    if (packet->getDataLength() > MAX_ETHERNET_DATA_BYTES)
        throw cRuntimeError("packet from higher layer (%d bytes) exceeds maximum Ethernet payload length (%d)", (int)packet->getByteLength(), MAX_ETHERNET_DATA_BYTES);

    //totalFromHigherLayer++;
    //emit(encapPkSignal, packet);

    // Creates MAC header information and encapsulates received higher layer data
    // with this information and transmits resultant frame to lower layer

    // create Ethernet frame, fill it in from Ieee802Ctrl and encapsulate msg in it
    EV_DETAIL << "Encapsulating higher layer packet `" << packet->getName() << "' for MAC\n";

    int typeOrLength = -1;
    if (!useSNAP) {
        auto protocolTag = packet->findTag<PacketProtocolTag>();
        if (protocolTag) {
            const Protocol *protocol = protocolTag->getProtocol();
            if (protocol) {
                int ethType = ProtocolGroup::ethertype.findProtocolNumber(protocol);
                if (ethType != -1)
                    typeOrLength = ethType;
            }
        }
    }
    if (typeOrLength == -1) {
        encapsulate(packet);
        typeOrLength = packet->getByteLength();
    }
    auto macAddressReq = packet->getTag<MacAddressReq>();
    const auto& ethHeader = makeShared<EthernetMacHeader>();
    ethHeader->setSrc(macAddressReq->getSrcAddress());    // if blank, will be filled in by MAC
    ethHeader->setDest(macAddressReq->getDestAddress());
    ethHeader->setTypeOrLength(typeOrLength);
    packet->insertAtFront(ethHeader);

    addPaddingAndFcs(packet, fcsMode);

    packet->addTagIfAbsent<PacketProtocolTag>()->setProtocol(&Protocol::ethernetMac);
    EV_INFO << "Sending " << packet << " to lower layer.\n";
    //send(packet, "lowerLayerOut");
}

void EthernetLayerApp::encapsulate(Packet *frame)
{
    auto protocolTag = frame->findTag<PacketProtocolTag>();
    const Protocol *protocol = protocolTag ? protocolTag->getProtocol() : nullptr;
    int ethType = -1;
    int snapOui = -1;
    if (protocol) {
        ethType = ProtocolGroup::ethertype.findProtocolNumber(protocol);
        if (ethType == -1)
            snapOui = ProtocolGroup::snapOui.findProtocolNumber(protocol);
    }
    if (ethType != -1 || snapOui != -1) {
        const auto& snapHeader = makeShared<Ieee8022LlcSnapHeader>();
        if (ethType != -1) {
            snapHeader->setOui(0);
            snapHeader->setProtocolId(ethType);
        }
        else {
            snapHeader->setOui(snapOui);
            snapHeader->setProtocolId(-1);      //FIXME get value from a tag (e.g. protocolTag->getSubId() ???)
        }
        frame->insertAtFront(snapHeader);
    }
    else {
        const auto& llcHeader = makeShared<Ieee8022LlcHeader>();
        int sapData = ProtocolGroup::ieee8022protocol.findProtocolNumber(protocol);
        if (sapData != -1) {
            llcHeader->setSsap((sapData >> 8) & 0xFF);
            llcHeader->setDsap(sapData & 0xFF);
            llcHeader->setControl(3);
        }
        else {
            auto sapReq = frame->getTag<Ieee802SapReq>();
            llcHeader->setSsap(sapReq->getSsap());
            llcHeader->setDsap(sapReq->getDsap());
            llcHeader->setControl(3);       //TODO get from sapTag
        }
        frame->insertAtFront(llcHeader);
    }
    frame->addTagIfAbsent<PacketProtocolTag>()->setProtocol(&Protocol::ieee8022);
}

void EthernetLayerApp::addPaddingAndFcs(Packet *packet, FcsMode fcsMode, B requiredMinBytes)
{
    B paddingLength = requiredMinBytes - ETHER_FCS_BYTES - B(packet->getByteLength());
    if (paddingLength > B(0)) {
        const auto& ethPadding = makeShared<EthernetPadding>();
        ethPadding->setChunkLength(paddingLength);
        packet->insertAtBack(ethPadding);
    }
    addFcs(packet, fcsMode);
}

void EthernetLayerApp::addFcs(Packet *packet, FcsMode fcsMode)
{
    const auto& ethFcs = makeShared<EthernetFcs>();
    ethFcs->setFcsMode(fcsMode);

    // calculate Fcs if needed
    if (fcsMode == FCS_COMPUTED) {
        auto ethBytes = packet->peekDataAsBytes();
        auto bufferLength = B(ethBytes->getChunkLength()).get();
        auto buffer = new uint8_t[bufferLength];
        // 1. fill in the data
        ethBytes->copyToBuffer(buffer, bufferLength);
        // 2. compute the FCS
        auto computedFcs = ethernetCRC(buffer, bufferLength);
        delete [] buffer;
        ethFcs->setFcs(computedFcs);
    }

    packet->insertAtBack(ethFcs);
}

MacAddress EthernetLayerApp::resolveDestMacAddress()
{
    MacAddress destMacAddress;
    const char *destAddress = par("destAddress");
    if (destAddress[0]) {
        if (!destMacAddress.tryParse(destAddress))
            destMacAddress = L3AddressResolver().resolve(destAddress, L3AddressResolver::ADDR_MAC).toMac();
    }
    return destMacAddress;
}

} //namespace
