//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __NESTING_SDNAPP_H_
#define __NESTING_SDNAPP_H_

#include <omnetpp.h>
#include <string.h>

#include "inet/common/packet/Packet.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "inet/common/packet/chunk/ByteCountChunk.h"
#include "inet/common/TimeTag_m.h"
#include "inet/common/ProtocolTag_m.h"
#include "inet/linklayer/common/Ieee802SapTag_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/linklayer/common/FcsMode_m.h"
#include "inet/linklayer/ieee8022/Ieee8022Llc.h"
#include "inet/common/checksum/EthernetCRC.h"
#include "inet/common/Topology.h"

#include "nesting/application/ethernet/VlanEtherTrafGenSched.h"
#include "nesting/linklayer/vlan/EnhancedVlanTag_m.h"

using namespace omnetpp;
using namespace inet;

namespace nesting {

/**
 * TODO - Generated class
 */
class SdnApp : public cSimpleModule
{
  private:
    // Parameters from NED file
    cPar* vlanTagEnabled;
    cPar* pcp;
    cPar* dei;
    cPar* vid;
  protected:
    cPar *packetLength = nullptr;
    int ssap = -1;
    int dsap = -1;
    MacAddress destMacAddress;
    bool useSNAP;    // true: generate EtherFrameWithSNAP, false: generate EthernetIIFrame
    FcsMode fcsMode = FCS_MODE_UNDEFINED;
    int neighborhoodCounter = 0;

    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual MacAddress resolveDestMacAddress(char *destAddressVar);
    virtual void processPacketFromHigherLayer(Packet *msg);
    virtual void encapsulate(Packet *frame);

  public:
      static void addPaddingAndFcs(Packet *packet, FcsMode fcsMode, B requiredMinByteLength = MIN_ETHERNET_FRAME_BYTES);
      static void addFcs(Packet *packet, FcsMode fcsMode);

  private:
      void sendMessageToSwitches(cMessage *msg);
      void generateTopology(cMessage *msg);
      void handleFailure(cMessage *msg);
      void handleStatisticsInfo(cMessage *msg);
};

} //namespace

#endif
