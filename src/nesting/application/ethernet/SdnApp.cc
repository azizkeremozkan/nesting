//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "SdnApp.h"

namespace nesting {

Define_Module(SdnApp);

void SdnApp::initialize()
{
    // TODO - Generated method body
    packetLength = &par("packetLength");
    vlanTagEnabled = &par("vlanTagEnabled");
    pcp = &par("pcp");
    dei = &par("dei");
    vid = &par("vid");
    useSNAP = par("useSNAP");
    fcsMode = parseFcsMode(par("fcsMode"));
    // deneme amaçlı mesajlar. her anahtara bir tane.
    //simtime_t delay = SimTime(90, SIMTIME_US);
    //scheduleAt(simTime() + delay, new cMessage("send message to switchA", 1));
    //delay = SimTime(100, SIMTIME_US);
    //scheduleAt(simTime() + delay, new cMessage("send message to switchB", 2));
    //delay = SimTime(110, SIMTIME_US);
    //scheduleAt(simTime() + delay, new cMessage("send message to switchC", 3));
    //delay = SimTime(120, SIMTIME_US);
    //scheduleAt(simTime() + delay, new cMessage("send message to switchD", 4));
    //delay = SimTime(130, SIMTIME_US);
    //scheduleAt(simTime() + delay, new cMessage("send message to switchE", 5));
    //delay = SimTime(140, SIMTIME_US);
    //scheduleAt(simTime() + delay, new cMessage("send message to switchF", 6));
}

void SdnApp::handleMessage(cMessage *msg)
{
    // TODO - Generated method body
    EV_INFO << "SdnApp handleMessage icinde" <<  std::endl;
    if (msg->isSelfMessage())
    {
        EV_INFO << "SdnApp self message icinde" <<  std::endl;
        sendMessageToSwitches(msg);
    }
    else
    {
        EV_INFO << "SdnApp self message icinde degil" <<  std::endl;
        if(msg->getKind() == 1001){ //komşuluk bilgisi aldı, tip 1001
            EV_INFO << "SdnApp komsuluk bilgisi aldi" << std::endl;
            // 6 tane aldığında çizgeyi oluştur
            neighborhoodCounter++;
            if(neighborhoodCounter == 6){
                neighborhoodCounter = 0;   // bir daha kullanılmayacak ama yine de sıfırladım
                generateTopology(msg);
            }
        } else if(msg->getKind() == 1002){ //arıza bilgisi aldı, tip 1002
            EV_INFO << "SdnApp ariza bilgisi aldi" << std::endl;
            handleFailure(msg);
        } else if(msg->getKind() == 1003){ //istatistik bilgisi aldı, tip 1003
            EV_INFO << "İstatistik bilgisi aldi" << std::endl;
            handleStatisticsInfo(msg);
        }
    }
}

void SdnApp::sendMessageToSwitches(cMessage *msg)
{
    char *destAddressVar;
    if(msg->getKind() == 1){
        destAddressVar = "00-00-00-00-02-06";
    }else if(msg->getKind() == 2){
        destAddressVar = "00-00-00-00-03-05";
    }else if(msg->getKind() == 3){
        destAddressVar = "00-00-00-00-04-05";
    }else if(msg->getKind() == 4){
        destAddressVar = "00-00-00-00-05-03";
    }else if(msg->getKind() == 5){
        destAddressVar = "00-00-00-00-06-03";
    }else if(msg->getKind() == 6){
        destAddressVar = "00-00-00-00-07-05";
    }else{

    }

    char msgname[40] = "Message from SDN Controller";
    Packet *datapacket = new Packet(msgname, IEEE802CTRL_DATA);
    datapacket->setKind(1002); // sdn denetleyici sadece arıza durumu için mesaj gönderir anahtarlara
    long len = packetLength->intValue();
    const auto& payload = makeShared<ByteCountChunk>(B(len));
    // set creation time
    auto timeTag = payload->addTag<CreationTimeTag>();
    timeTag->setCreationTime(simTime());

    datapacket->insertAtBack(payload);
    datapacket->removeTagIfPresent<PacketProtocolTag>();
    datapacket->addTagIfAbsent<PacketProtocolTag>()->setProtocol(VlanEtherTrafGenSched::L2_PROTOCOL);
    // TODO check which protocol to insert
    auto sapTag = datapacket->addTagIfAbsent<Ieee802SapReq>();
    sapTag->setSsap(ssap);
    sapTag->setDsap(dsap);

    // create control info for encap modules
    auto macTag = datapacket->addTag<MacAddressReq>();
    destMacAddress = resolveDestMacAddress(destAddressVar);
    macTag->setDestAddress(destMacAddress);

    // create VLAN control info
    if (vlanTagEnabled->boolValue()) {
        EnhancedVlanReq* vlanReq = datapacket->addTag<EnhancedVlanReq>();
        vlanReq->setPcp(pcp->intValue());
        vlanReq->setDe(dei->boolValue());
        vlanReq->setVlanId(vid->intValue());
    }

    EV_TRACE << getFullPath() << ": Send packet `" << datapacket->getName()
                    << "' dest=" << macTag->getDestAddress() << " length="
                    << datapacket->getBitLength() << "B type="
                    << IEEE802CTRL_DATA << " vlan-tagged="
                    << vlanTagEnabled->boolValue();
    if (vlanTagEnabled->boolValue()) {
        EV_TRACE << " pcp=" << pcp->intValue() << " dei=" << dei->boolValue() << " vid=" << vid->intValue();
    }
    EV_TRACE << endl;

    //emit(packetSentSignal, datapacket);
    processPacketFromHigherLayer(datapacket);
    send(datapacket, "out");
}

void SdnApp::generateTopology(cMessage *msg)
{
    EV_INFO << "Generate topology içinde" << std::endl;
    // çizge burada oluşturuluyor
    // Topoloji objesi
    auto topology = new Topology();

    // INET'te bütün node'lar @netwokNode olarak damgalı. Bu özelliğe göre topolojiyi çıkardık.
    topology->extractByProperty("networkNode");

    // Topolojideki düğüm sayısı
    EV_INFO << "Total nodes are " << topology->getNumNodes() << "\n";

    // Düğümlerin adları
    for(int i = 0; i < topology->getNumNodes(); i++){
        EV_INFO << topology->getNode(i)->getModule() << "\n";
    }

    // Modüllere gelen bağlantı sayısı
    for(int i = 0; i < topology->getNumNodes(); i++){
        EV_INFO << topology->getNode(i)->getModule() << " modülüne gelen bağlantı sayısı " << topology->getNode(i)->getNumInLinks() << "\n";
    }

    // Modüllerden çıkan bağlantı sayısı
    for(int i = 0; i < topology->getNumNodes(); i++){
        EV_INFO << topology->getNode(i)->getModule() << " modülünden çıkan bağlantı sayısı " << topology->getNode(i)->getNumOutLinks() << "\n";
    }

    // Modüller hangi modüllere bağlı
    for(int i = 0; i < topology->getNumNodes(); i++){
        for(int j = 0; j < topology->getNode(i)->getNumOutLinks(); j++){
            EV_INFO << topology->getNode(i)->getModule() << " modülü " << topology->getNode(i)->getLinkOut(j)->getRemoteNode()->getModule() << " modülüne bağlı" << "\n";
        }
    }
}

void SdnApp::handleFailure(cMessage *msg)
{
    EV_INFO << "Handle failure içinde" << std::endl;
    // arızanın giderilmesi için hesaplama yapılıyor
    // ve yeni bir konfigürasyon bulunuyor
    // bu yeni konfigürasyon için anahtarlara akış kuralı değişiklikleri gönderiliyor
    // bu hata switchD'den gelmişti (örnek olarak onu seçtik)
    // onun üstünden geçen trafikleri switchE üzerinden gönderme yolunu seçebiliriz
    // bunun için de önce switchF'ye sonra switchE'ye ve sonra switchA'ya kural değişikliği emri göndeririz
    // örnek olsun diye gönderelim boş mesajları
    scheduleAt(simTime(), new cMessage("send message to switchF", 6));
    // burada switchF'den değişikliği yaptım anlamında cevap alınca diğerlerine göndermeli
    // değişiklik emirlerini. onu şimdilik delay ile yapalım.
    simtime_t delay = SimTime(15, SIMTIME_US);
    scheduleAt(simTime() + delay, new cMessage("send message to switchE", 5));
    delay = SimTime(30, SIMTIME_US);
    scheduleAt(simTime() + delay, new cMessage("send message to switchA", 1));
    delay = SimTime(45, SIMTIME_US);
    scheduleAt(simTime() + delay, new cMessage("send message to switchD", 4));
    scheduleAt(simTime() + delay, new cMessage("send message to switchC", 3));
}

void SdnApp::handleStatisticsInfo(cMessage *msg)
{
    EV_INFO << "Handle statistics info içinde" << std::endl;
    cMsgPar newStatisticsPar = msg->par(0);
    long totalMessageCount = newStatisticsPar.longValue();
    std::string tempStr(newStatisticsPar.getName());
    EV_INFO << "anahtarlama işlemi gören paket sayısı: " << std::to_string(totalMessageCount) << ", anahtar adı: " << tempStr.substr(0, tempStr.find("-")) << std::endl;
}

void SdnApp::processPacketFromHigherLayer(Packet *packet)
{
    delete packet->removeTagIfPresent<DispatchProtocolReq>();
    if (packet->getDataLength() > MAX_ETHERNET_DATA_BYTES)
        throw cRuntimeError("packet from higher layer (%d bytes) exceeds maximum Ethernet payload length (%d)", (int)packet->getByteLength(), MAX_ETHERNET_DATA_BYTES);

    //totalFromHigherLayer++;
    //emit(encapPkSignal, packet);

    // Creates MAC header information and encapsulates received higher layer data
    // with this information and transmits resultant frame to lower layer

    // create Ethernet frame, fill it in from Ieee802Ctrl and encapsulate msg in it
    EV_DETAIL << "Encapsulating higher layer packet `" << packet->getName() << "' for MAC\n";

    int typeOrLength = -1;
    if (!useSNAP) {
        auto protocolTag = packet->findTag<PacketProtocolTag>();
        if (protocolTag) {
            const Protocol *protocol = protocolTag->getProtocol();
            if (protocol) {
                int ethType = ProtocolGroup::ethertype.findProtocolNumber(protocol);
                if (ethType != -1)
                    typeOrLength = ethType;
            }
        }
    }
    if (typeOrLength == -1) {
        encapsulate(packet);
        typeOrLength = packet->getByteLength();
    }
    auto macAddressReq = packet->getTag<MacAddressReq>();
    const auto& ethHeader = makeShared<EthernetMacHeader>();
    ethHeader->setSrc(macAddressReq->getSrcAddress());    // if blank, will be filled in by MAC
    ethHeader->setDest(macAddressReq->getDestAddress());
    ethHeader->setTypeOrLength(typeOrLength);
    packet->insertAtFront(ethHeader);

    addPaddingAndFcs(packet, fcsMode);

    packet->addTagIfAbsent<PacketProtocolTag>()->setProtocol(&Protocol::ethernetMac);
    EV_INFO << "Sending " << packet << " to lower layer.\n";
    //send(packet, "lowerLayerOut");
}

void SdnApp::encapsulate(Packet *frame)
{
    auto protocolTag = frame->findTag<PacketProtocolTag>();
    const Protocol *protocol = protocolTag ? protocolTag->getProtocol() : nullptr;
    int ethType = -1;
    int snapOui = -1;
    if (protocol) {
        ethType = ProtocolGroup::ethertype.findProtocolNumber(protocol);
        if (ethType == -1)
            snapOui = ProtocolGroup::snapOui.findProtocolNumber(protocol);
    }
    if (ethType != -1 || snapOui != -1) {
        const auto& snapHeader = makeShared<Ieee8022LlcSnapHeader>();
        if (ethType != -1) {
            snapHeader->setOui(0);
            snapHeader->setProtocolId(ethType);
        }
        else {
            snapHeader->setOui(snapOui);
            snapHeader->setProtocolId(-1);      //FIXME get value from a tag (e.g. protocolTag->getSubId() ???)
        }
        frame->insertAtFront(snapHeader);
    }
    else {
        const auto& llcHeader = makeShared<Ieee8022LlcHeader>();
        int sapData = ProtocolGroup::ieee8022protocol.findProtocolNumber(protocol);
        if (sapData != -1) {
            llcHeader->setSsap((sapData >> 8) & 0xFF);
            llcHeader->setDsap(sapData & 0xFF);
            llcHeader->setControl(3);
        }
        else {
            auto sapReq = frame->getTag<Ieee802SapReq>();
            llcHeader->setSsap(sapReq->getSsap());
            llcHeader->setDsap(sapReq->getDsap());
            llcHeader->setControl(3);       //TODO get from sapTag
        }
        frame->insertAtFront(llcHeader);
    }
    frame->addTagIfAbsent<PacketProtocolTag>()->setProtocol(&Protocol::ieee8022);
}

void SdnApp::addPaddingAndFcs(Packet *packet, FcsMode fcsMode, B requiredMinBytes)
{
    B paddingLength = requiredMinBytes - ETHER_FCS_BYTES - B(packet->getByteLength());
    if (paddingLength > B(0)) {
        const auto& ethPadding = makeShared<EthernetPadding>();
        ethPadding->setChunkLength(paddingLength);
        packet->insertAtBack(ethPadding);
    }
    addFcs(packet, fcsMode);
}

void SdnApp::addFcs(Packet *packet, FcsMode fcsMode)
{
    const auto& ethFcs = makeShared<EthernetFcs>();
    ethFcs->setFcsMode(fcsMode);

    // calculate Fcs if needed
    if (fcsMode == FCS_COMPUTED) {
        auto ethBytes = packet->peekDataAsBytes();
        auto bufferLength = B(ethBytes->getChunkLength()).get();
        auto buffer = new uint8_t[bufferLength];
        // 1. fill in the data
        ethBytes->copyToBuffer(buffer, bufferLength);
        // 2. compute the FCS
        auto computedFcs = ethernetCRC(buffer, bufferLength);
        delete [] buffer;
        ethFcs->setFcs(computedFcs);
    }

    packet->insertAtBack(ethFcs);
}

MacAddress SdnApp::resolveDestMacAddress(char *destAddressVar)
{
    MacAddress destMacAddress;
    const char *destAddress = destAddressVar;
    if (destAddress[0]) {
        if (!destMacAddress.tryParse(destAddress))
            destMacAddress = L3AddressResolver().resolve(destAddress, L3AddressResolver::ADDR_MAC).toMac();
    }
    return destMacAddress;
}

} //namespace
